import cv2 as cv

#Dentro de -> cv.CascadeClassifier() agrega la ruta especifica donde se encuentre el archivo: haarcascade_frontalface_default.xml
ruidos = cv.CascadeClassifier('C:/Users/epict/Downloads/opencv-4.x/data/haarcascades/haarcascade_frontalface_default.xml')
print(ruidos)

camara = cv.VideoCapture(0)#numero de camaras, se puede meter un mp4

while True:
    _,captura = camara.read()
    grises = cv.cvtColor(captura, cv.COLOR_BGR2GRAY)

    cara=ruidos.detectMultiScale(grises,1.5,3,5)

    for (x,y,e1,e2) in cara:
        cv.rectangle(captura,(x,y),(x+e1,y+e2),(0,255,0), 2)#(img, inicio, final, color, tamaño)

        cv.imshow("VER ROSTRO", captura)#(string, img)
    if cv.waitKey(1) == ord('a'):
     break

camara.release()
cv.destroyAllWindows()