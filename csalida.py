#capa salida
import cv2 as cv
import os 
import imutils

#En la variable -> dataruta, coloca la ruta de la carpeta /Facial/datos, para obtener las carpetas de los modelos almacenados
dataruta = 'C:/Users/epict/OneDrive/Documentos/Cosas Sexto Semestre/Taller Redes Python/Facial/datos'
listadata = os.listdir(dataruta)

entrenamientoEFR = cv.face.EigenFaceRecognizer_create() 
entrenamientoEFR.read('entrenamientoefr.xml')

#Dentro de -> cv.CascadeClassifier() agrega la ruta especifica donde se encuentre el archivo: haarcascade_frontalface_default.xml  
ruidos = cv.CascadeClassifier('C:/Users/epict/Downloads/opencv-4.x/data/haarcascades/haarcascade_frontalface_default.xml')
print(ruidos)

camara = cv.VideoCapture(0)#numero de camaras, se puede meter un mp4
# camara = cv.VideoCapture('Aqui iria la direcion de un video')


while True:
    resp,captura = camara.read()

    if resp == False:break

    captura = imutils.resize(captura,width=640)#cambia el tamanño de pixeles
    grises = cv.cvtColor(captura, cv.COLOR_BGR2GRAY)

    idcaptura = grises.copy() # captura y guarda cada punto de pixel

    cara=ruidos.detectMultiScale(grises,1.3,5)

    for(x,y,e1,e2) in cara:
        rostroCaptura = idcaptura[y:y+e2, x:x+e1] # fragmentos del rostro
        rostroCaptura = cv.resize(rostroCaptura,(160,160),interpolation=cv.INTER_CUBIC)
        resultado = entrenamientoEFR.predict(rostroCaptura)
        cv.putText(captura, '{}'.format(resultado), (x,y-5), 1, 1.3, (255,0,0), 1, cv.LINE_AA)
        # cv.rectangle(captura, (x,y),(x+e1, y+e2),(255,0,0), 2)

        if resultado[1]<9000:                                                       #2 o 1, 1.1 o 1.3
            cv.putText(captura, '{}'.format(listadata[resultado[0]]), (x,y-20), 2, 1.3, (255,0,0), 1, cv.LINE_AA)
            cv.rectangle(captura, (x,y),(x+e1, y+e2),(255,0,0), 2)
        else:
            cv.putText(captura, "ROSTRO NO IDENTIFICADO!!", (x,y-20), 2, 1.3, (255,0,0), 1, cv.LINE_AA)
            cv.rectangle(captura, (x,y),(x+e1, y+e2),(255,0,0), 2) 

    cv.imshow("RESULTADO",captura)
    if cv.waitKey(1)==ord('a'):
        break

camara.release()
cv.destroyAllWindows()

#programar emociones
#Detectar si un rostro esta sonriendo, triste, enojado y asi
#Por cada emocion sera una carpeta
