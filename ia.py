
import cv2 as cv
import os #sirve para crear carpetas o archivos
import imutils
import sys

#Este codigo crea archivos y carpetas
#En la variable -> ruta, coloca la ruta de la carpeta /Facial, o donde se desee almacenar los modelos
carpetaDatos = "datos"
ruta = "C:/Users/epict/OneDrive/Documentos/Cosas Sexto Semestre/Taller Redes Python/Facial"
rutaModelo = ruta+"/"+carpetaDatos
if not os.path.exists(rutaModelo):
    os.makedirs(rutaModelo)

modelo = input("Escribe el nombre de rostro (carpeta): ")

rutaCompleta = rutaModelo+"/"+modelo
if os.path.exists(rutaCompleta):
    sys.exit()
    print("El nombre de rostros (carpeta) se repite, intentalo otra vez")

#Dentro de -> cv.CascadeClassifier() agrega la ruta especifica donde se encuentre el archivo: haarcascade_frontalface_default.xml  
ruidos = cv.CascadeClassifier('C:/Users/epict/Downloads/opencv-4.x/data/haarcascades/haarcascade_frontalface_default.xml')
print(ruidos)

camara = cv.VideoCapture(0)#numero de camaras, se puede meter un mp4
# camara = cv.VideoCapture('Aqui iria la direcion de un video')

id=0 #para id de cada foto/img


#Hace que pensen menos las imagenes = pip install imutils
while True:
    _,captura = camara.read()
    captura = imutils.resize(captura,width=640)#cambia el tamanño de pixeles
    grises = cv.cvtColor(captura, cv.COLOR_BGR2GRAY)

    idcaptura = captura.copy() # captura y guarda cada punto de pixel

    cara=ruidos.detectMultiScale(grises,1.3,5)

    for (x,y,e1,e2) in cara:
        #En el ultimo parentesis puede que:0,255,0
        cv.rectangle(captura,(x,y),(x+e1,y+e2),(0,255,0), 2)#(img, inicio, final, color, tamaño)
        rostroCaptura = idcaptura[y:y+e2, x:x+e1] # fragmentos del rostro
        
        rostroCaptura = cv.resize(rostroCaptura,(160,160),interpolation=cv.INTER_CUBIC) 
        
        cv.imshow("VER ROSTRO", captura)#(string, img)
        cv.imwrite(rutaCompleta+'/foto_{}.jpg'.format(id), rostroCaptura)

        # cv.imwrite(rutaCompleta+'/foto_{}.jpg'.format(id, rostroCaptura))
        # ruta_imagen = rutaCompleta+'/foto_{}.jpg'.format(id)
        # cv.imwrite(ruta_imagen, rostroCaptura)


        id=id+1
    #if cv.waitkey(1) == ord('a'):
    if id==250:
        break


camara.release()
cv.destroyAllWindows()


#Aqui esta el python
#C:\Users\epict\AppData\Local\Programs\Python\Python311
#C:\Users\epict\AppData\Local\Programs\Python\Python311\python.exe

#Para instalar opencv
#pip install opencv-contrib-python opencv-python-headless
#Reconocimiento facial

# num = int(input("ingrese numero: "))
# print("Redes neuronales" ,num)

# lista = []
# # for i in lista:
# #     print(i) 

# for i  in range(num):
#     lista.append (int(input("ingrese numero: ")))
    
# print(lista)
#frontfacedefault.xml

#pip install --upgrade pip --user
#python install opencv --pip