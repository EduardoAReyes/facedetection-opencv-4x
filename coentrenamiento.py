#capa oculta
import cv2 as cv
import os
import numpy as np
#docs opencv

#En la variable -> dataruta, coloca la ruta de la carpeta /Facial/datos, para obtener las carpetas de los modelos almacenados
dataruta = 'C:/Users/epict/OneDrive/Documentos/Cosas Sexto Semestre/Taller Redes Python/Facial/datos'
listadata = os.listdir (dataruta)
print(listadata)

ids = []
rostrodata = []
id = 0
for carp in listadata:
    rutacompleta = dataruta+'/'+carp
    print('iniciando lectura de carpetas')
    for archivo in os.listdir(rutacompleta):
        ids.append(id)
        rostrodata.append(cv.imread(rutacompleta+'/'+archivo,0))

    id=id+1

#Aqui comienza la red neuronal con python y uso de opencv
# entrenamientoEFR = cv.face.EigenFaceRecognizer.create() 
entrenamientoEFR = cv.face.EigenFaceRecognizer_create() 
print('iniciando entrenamiento...Espere')
entrenamientoEFR.train(rostrodata, np.array(ids))
entrenamientoEFR.write('entrenamientoefr.xml') #Esto es red neuronal
print('Entrenamiento concluido...')

#Se va generar un xml
#Se va a entrenar un xml, y se puede usar para otro entrenamiento

