Pequeñas instrucciones para ejecutar este pequeño programa.

1.- Descomprime opencv-4.x.zip, guarda la ruta ya que se usara un archivo llamado: haarcascade_frontalface_default.xml

2.- Ejecutar el comando: python ia.py
Para capturar el rostro y almacenar los modelos.

3.- Ejecutar el comando: python coentrenamiento.py
Para ejecutar el entrenamiento y generar un archivo XML, el cual es nuestra red neuronal.

4.- Ejecutar el comando: python csalida.py
Para activar nuestra cámara y verificar el rostro.